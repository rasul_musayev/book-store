package az.ingress.users.config;

import az.ingress.users.config.SecurityProperties;
import az.ingress.users.dto.JwtCredentials;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Slf4j
@Service
@RequiredArgsConstructor
public class JwtService {

    private final SecurityProperties applicationProperties;
    private Key key;

    @Value("10000")
    private Long duration;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(applicationProperties
                .getJwtProperties()
                .getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken(JwtCredentials jwtCredentials) {
        log.info("Issue JWT token to {} for {} seconds",jwtCredentials,duration);
        final JwtBuilder jwtBuilder  = Jwts.builder()
                .setIssuedAt(new Date())
                .claim(JwtCredentials.Fields.id,jwtCredentials.getId())
                .claim(JwtCredentials.Fields.role,jwtCredentials.getRole())
                .claim(JwtCredentials.Fields.username,jwtCredentials.getUsername())
                .setExpiration(Date.from(Instant.now().plusSeconds(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }
}
