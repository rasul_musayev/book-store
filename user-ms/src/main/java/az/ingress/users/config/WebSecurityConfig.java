package az.ingress.users.config;

import az.ingress.users.entity.UserRole;
import az.ingress.users.filter.JwtAuthFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthFilterConfigurerAdapter adapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/registration")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/sign-in")
                .permitAll()
                .and()
//                .authorizeRequests()
//                .antMatchers("/user/api/**")
//                .permitAll()
//                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/registration/author")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/auth/registration/publisher")
                .permitAll()
                        .antMatchers(HttpMethod.GET,"/user/api/hello")
                                .hasAnyAuthority("ROLE_USER");


        http.apply(adapter);
        super.configure(http);
    }
}
