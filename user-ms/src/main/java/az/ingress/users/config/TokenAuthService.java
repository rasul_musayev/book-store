package az.ingress.users.config;

import az.ingress.users.config.JwtService;
import az.ingress.users.services.AuthService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class TokenAuthService implements AuthService {
    private static final String AUTH_HEADER = "Authorization";
    private static final String BEARER_AUTH_HEADER = "Bearer ";
    public static final String ROLES_CLAIM = "rules";

    private final JwtService jwtService;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest httpServletRequest) {
        return Optional.ofNullable(httpServletRequest.getHeader(AUTH_HEADER))
                .filter(this::isBearerAuth)
                .flatMap(this::getAuthenticationBearer);
    }

    private boolean isBearerAuth(String header) {
        return header.toLowerCase().startsWith(BEARER_AUTH_HEADER.toLowerCase());
    }

    private Optional<Authentication> getAuthenticationBearer(String header) {
        String token = header.substring(BEARER_AUTH_HEADER.length()).trim();
        Claims claims = jwtService.parseToken(token);
        log.trace("The claims parsed {}", claims);
        return claims.getExpiration().before(new Date())
                ? Optional.empty() : Optional.of(getAuthenticationBearer(claims));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
//        List<?> roles = claims.get(ROLES_CLAIM, List.class);
//        List<GrantedAuthority> authorityList = roles
//                .stream()
//                .map(a -> new SimpleGrantedAuthority(a.toString()))
//                .collect(Collectors.toList());
        var roles = claims.get(ROLES_CLAIM, String.class);
        List<GrantedAuthority> authorityList =
                List.of(new SimpleGrantedAuthority(roles.toString()));
        return new UsernamePasswordAuthenticationToken(claims.getSubject(), "", authorityList);
    }
}
