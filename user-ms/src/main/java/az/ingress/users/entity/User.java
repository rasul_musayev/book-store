package az.ingress.users.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String username;
    private String password;
    private String lastName;
    private String email;
    private Long phone;

    boolean isEnabled;
    boolean credentialsNonExpired;
    boolean isAccountNonLocked;
    boolean isAccountNonExpired;


    @Enumerated(EnumType.STRING)
    UserRole userRole;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        var authority = new SimpleGrantedAuthority(userRole.name());
        return Collections.singleton(authority);
    }
}

