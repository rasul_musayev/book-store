package az.ingress.users.entity;

public enum UserRole {
    ROLE_ADMIN,
    ROLE_PUBLISHER,
    ROLE_USER,
    ROLE_AUTHOR;


}
