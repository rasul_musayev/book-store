package az.ingress.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    private String username;
    private String firstname;
    private String lastname;
    private Long phoneNumber;
    private String password;
    private String email;
    private String role;


}
