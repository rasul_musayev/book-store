package az.ingress.users.rest;

import az.ingress.users.dto.UserDto;
import az.ingress.users.services.UserServices;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/user/api")
@RequiredArgsConstructor

public class UserController {
    private final UserServices userServices;

    @GetMapping("/hello")
    public String hello() {
        return "Heelo World";
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable Long id) {
        return userServices.getUserId(id);
    }

    @PostMapping("/create/user")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto createUser(@Valid @RequestBody UserDto userDto) {
        log.trace("Received request {}", userDto);
        return userServices.createUser(userDto);
    }

    @PutMapping("/update/{id}")
    public UserDto updateUser(@PathVariable Long id, @RequestBody UserDto userDto) {
        return userServices.updateUser(id, userDto);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        userServices.deleteUser(id);
    }

    @GetMapping("/list")
    public Page<UserDto> listUsers(Pageable pageable) {
        return userServices.getUserList(pageable);
    }

}