package az.ingress.users.rest;

import az.ingress.users.dto.LoginRequest;
import az.ingress.users.dto.LoginResponse;
import az.ingress.users.dto.RegisterDto;
import az.ingress.users.services.UserServices;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {
    private final UserServices userService;

    @PostMapping("/sign-in")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginDto) {
        log.info("Login request by user {}", loginDto.getUsername());
        return ResponseEntity.ok(userService.login(loginDto));
    }
    @PostMapping("/registration")
    public void register(@RequestBody RegisterDto request) {
        userService.register(request);
    }
    @PostMapping("/registration/author")
    public void registerAuthor(@RequestBody RegisterDto registerDto) {
        userService.registerAuthor(registerDto);
    }
    @PostMapping("/registration/publisher")
    public void registerPublisher(@RequestBody RegisterDto registerDto) {
        userService.registerPublisher(registerDto);
    }
}
