package az.ingress.users.services.impl;


import az.ingress.users.dto.*;
import az.ingress.users.entity.User;
import az.ingress.users.entity.UserRole;
import az.ingress.users.exception.UserNotFoundException;
import az.ingress.users.repository.UserRepository;
import az.ingress.users.config.JwtService;
import az.ingress.users.services.UserServices;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserServices {
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    @Override
    public UserDto getUserId(Long id) {
        User user = checkIfExits(id);
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public UserDto createUser(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    @Override
    public UserDto updateUser(Long id, UserDto userDto) {
        User update = update(checkIfExits(id), userDto);
        return modelMapper.map(userRepository.save(update), UserDto.class);
    }

    @Override
    public void deleteUser(Long id) {
        checkIfExits(id);
        userRepository.deleteById(id);
    }

    @Override
    public Page<UserDto> getUserList(Pageable pageable) {
        return userRepository.findAll(pageable)
                .map(user -> modelMapper.map(user, UserDto.class));
    }

    @Override
    public void register(RegisterDto registerDto) {
        Optional<User> userName = userRepository.findByUsername(registerDto.getUsername());
        if (userName.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        userRepository.save(createUser(registerDto, UserRole.ROLE_USER));
    }

    @Override
    public void registerAuthor(RegisterDto registerDto) {
        Optional<User> userName = userRepository.findByUsername(registerDto.getUsername());
        if (userName.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        userRepository.save(createUser(registerDto, UserRole.ROLE_AUTHOR));
    }

    @Override
    public void registerPublisher(RegisterDto registerDto) {
        Optional<User> userName = userRepository.findByUsername(registerDto.getUsername());
        if (userName.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        createUser(registerDto, UserRole.ROLE_PUBLISHER);
        userRepository.save(createUser(registerDto, UserRole.ROLE_PUBLISHER));

    }

    @Override
    public LoginResponse login(@NotNull LoginRequest loginDto) {
        //burda username gore user-i getirir
        User userDetails;
        userDetails = (User) loadUserByUsername(loginDto.getUsername());
        //databasedeki password
        boolean matches = passwordEncoder.matches(loginDto.getPassword(),
                userDetails.getPassword());
        if (!matches) {
            throw new UserNotFoundException("User not found");
        }
        return LoginResponse.builder().jwt(jwtService.issueToken(JwtCredentials.builder()
                .role(userDetails.getUserRole()).username(userDetails.getUsername()).build())).build();
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException("User with username " + username + " not found"));
    }

    private User checkIfExits(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User not found"));
    }

    private User update(User user, UserDto userDto) {
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUserRole(userDto.getUserRole());
        return user;
    }

    private User createUser(RegisterDto registerDto, UserRole userRole) {
        return User.builder().username(registerDto.getUsername())
                .password(passwordEncoder.encode(registerDto.getPassword()))
                .firstName(registerDto.getFirstname())
                .lastName(registerDto.getLastname())
                .phone(registerDto.getPhoneNumber())
                .email(registerDto.getEmail())
                .userRole(userRole)
                .credentialsNonExpired(true)
                .isAccountNonExpired(true)
                .isEnabled(true)
                .isAccountNonLocked(true)
                .build();
    }
}
