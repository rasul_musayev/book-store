package az.ingress.book.services;

import az.ingress.book.dto.BookDto;
import az.ingress.book.dto.BookRequestDto;
import az.ingress.book.dto.SearchRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface BookService {

    BookDto getById(Long id);

    BookDto create(BookRequestDto bookDto);

    Page<BookDto> getAllBooks(Pageable pageable);

    BookDto updateBook(Long id, BookDto bookDto);

    void deleteBook(Long id);

    Page<BookDto> searchByTitle(SearchRequestDto searchRequestDto,Pageable pageable);

    Page<BookDto> getBooksByAuthorId(Long authorId,Pageable pageable);

    Page<BookDto> getBooksByPublisherId(Long publisherId,Pageable pageable);

}
