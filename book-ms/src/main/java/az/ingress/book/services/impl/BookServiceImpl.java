package az.ingress.book.services.impl;

import az.ingress.book.dto.BookDto;
import az.ingress.book.dto.BookRequestDto;
import az.ingress.book.dto.PublisherDto;
import az.ingress.book.dto.SearchRequestDto;
import az.ingress.book.entity.Author;
import az.ingress.book.entity.Book;
import az.ingress.book.entity.Publisher;
import az.ingress.book.exception.AuthorNotFoundException;
import az.ingress.book.exception.BookNotFoundException;
import az.ingress.book.exception.PublisherNotFoundException;
import az.ingress.book.repository.AuthorRepository;
import az.ingress.book.repository.BookRepository;
import az.ingress.book.repository.PublisherRepository;
import az.ingress.book.services.BookService;
import az.ingress.book.specification.BookSpecifications;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;
    private final AuthorRepository authorRepository;
    private final PublisherRepository publisherRepository;

    @Override
    public BookDto getById(Long id) {
        return modelMapper.map(checkIfExists(id), BookDto.class);
    }

    @Override
    public BookDto create(BookRequestDto bookDto) {
        Book book = modelMapper.map(bookDto, Book.class);
        createBook(book, bookDto);
        return modelMapper.map(bookRepository.save(book), BookDto.class);
    }

    @Override
    public Page<BookDto> getAllBooks(Pageable pageable) {
        return bookRepository
                .findAll(pageable)
                .map(book -> modelMapper.map(book, BookDto.class));
    }

    @Override
    public BookDto updateBook(Long id, BookDto bookDto) {
        Book update = update(checkIfExists(id), bookDto);
        return modelMapper.map(bookRepository.save(update), BookDto.class);
    }

    @Override
    public void deleteBook(Long id) {
        checkIfExists(id);
        bookRepository.deleteById(id);
    }

    @Override
    public Page<BookDto> searchByTitle(SearchRequestDto searchRequestDto, Pageable pageable) {
        Specification<Book> spec = Specification.where(null);
        if (searchRequestDto.getPage() != null) {
            spec = spec.and(BookSpecifications.hasPage(searchRequestDto.getPage()));
        }
        if (searchRequestDto.getName() != null) {
            spec = spec.and(BookSpecifications.hasNameLike(searchRequestDto.getName()));
        }
        if (searchRequestDto.getDescription() != null) {
            spec = spec.and(BookSpecifications.hasDescriptionLike(searchRequestDto.getDescription()));
        }
        if (searchRequestDto.getPrice() != null) {
            spec = spec.and(BookSpecifications.hasPrice(searchRequestDto.getPrice()));
        }
        if (searchRequestDto.getAuthorName() != null) {
            spec = spec.and(BookSpecifications.hasAuthorNameLike(searchRequestDto.getAuthorName()));
        }
        if (searchRequestDto.getPublisherName() != null) {
            spec = spec.and(BookSpecifications.hasPublisherNameLike(searchRequestDto.getPublisherName()));
        }
        return bookRepository.findAll(spec, pageable).map(book ->
                modelMapper.map(book, BookDto.class));
    }

    @Override
    public Page<BookDto> getBooksByAuthorId(Long authorId, Pageable pageable) {
        return bookRepository.findBooksByAuthorId(authorId, pageable)
                .map(book -> modelMapper.map(book, BookDto.class));
    }

    @Override
    public Page<BookDto> getBooksByPublisherId(Long publisherId, Pageable pageable) {
        return bookRepository.findBooksByAuthorId(publisherId, pageable)
                .map(book -> modelMapper.map(book, BookDto.class));
    }

    private Book createBook(Book book, BookRequestDto bookDto) {
        book.setAuthor(getAuthor(bookDto.getAuthorId()));
        book.setPublisher(getPublisher(bookDto.getPublisherId()));
        return book;
    }

    private Book update(Book book, BookDto bookDto) {
        book.setPrice(bookDto.getPrice());
        book.setPage(bookDto.getPage());
        book.setAuthor(modelMapper.map(bookDto.getAuthor(), Author.class));
        bookDto.setPublisher(modelMapper.map(bookDto.getPublisher(), PublisherDto.class));
        return book;
    }

    private Book checkIfExists(Long id) {
        return bookRepository.findById(id).orElseThrow(() ->
                new BookNotFoundException("ID not found"));
    }

    private Publisher getPublisher(Long id) {
        return publisherRepository.findById(id).orElseThrow(() ->

                new PublisherNotFoundException("Publisher not found"));
    }

    private Author getAuthor(Long id) {
        return authorRepository.findById(id).orElseThrow(() ->
                new AuthorNotFoundException("Author not found"));

    }
}

