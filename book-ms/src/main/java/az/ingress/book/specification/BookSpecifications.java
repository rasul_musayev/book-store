package az.ingress.book.specification;

import az.ingress.book.entity.Book;
import org.springframework.data.jpa.domain.Specification;

public class BookSpecifications {
    public static Specification<Book> hasPage(Integer page) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("page"), page);
    }

    public static Specification<Book> hasNameLike(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("name"),
                "%" + name + "%");
    }

    public static Specification<Book> hasDescriptionLike(String description) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("description"),
                "%" + description + "%");
    }

    public static Specification<Book> hasPrice(Double price) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("price"), price);
    }

    public static Specification<Book> hasAuthorNameLike(String authorName) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("authorName"),
                "%" + authorName + "%");
    }

    public static Specification<Book> hasPublisherNameLike(String publisherName) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("publisherName"),
                "%" + publisherName + "%");
    }
}

