package az.ingress.book.repository;

import az.ingress.book.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PublisherRepository extends JpaRepository<Publisher, Long> {

}

