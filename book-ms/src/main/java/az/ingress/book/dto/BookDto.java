package az.ingress.book.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookDto {
    private Long id;
    private Integer page;
    private String name;
    private String title;
    private Double price;
    private String description;
    private AuthorDto author;
    private PublisherDto publisher;

}
