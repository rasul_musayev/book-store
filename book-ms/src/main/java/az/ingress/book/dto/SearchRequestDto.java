package az.ingress.book.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchRequestDto {
    private Integer page;
    private String name;
    private String description;
    private Double price;
    private String authorName;
    private String publisherName;


}
