package az.ingress.book.config;

import az.ingress.book.filter.JwtAuthRequestFilter;
import az.ingress.users.entity.UserRole;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static az.ingress.users.entity.UserRole.ROLE_USER;


@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    @SneakyThrows
    protected void configure(HttpSecurity http)  {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/books/all-books")
                        .hasAnyRole("role_user");
//                        .hasAnyAuthority(UserRole.ROLE_USER.name());
        super.configure(http);
    }
}
