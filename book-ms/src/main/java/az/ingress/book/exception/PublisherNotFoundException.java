package az.ingress.book.exception;

public class PublisherNotFoundException extends RuntimeException {
    public PublisherNotFoundException(String message){
        super(message);
    }
}
