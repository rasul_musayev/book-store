package az.ingress.book.rest;

import az.ingress.book.dto.BookDto;
import az.ingress.book.dto.BookRequestDto;
import az.ingress.book.dto.SearchRequestDto;
import az.ingress.book.services.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto createBook(@RequestBody BookRequestDto bookDto) {
        return bookService.create(bookDto);
    }

    @PutMapping("/update/{id}")
    public BookDto updateBook(@PathVariable Long id, @RequestBody BookDto bookDto) {
        return bookService.updateBook(id, bookDto);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
    }

    @GetMapping("/all-books")
    public Page<BookDto> getMyBooks(Pageable pageable, Authentication authentication) {
        System.out.println("---"+authentication);
//        System.out.println("---"+authentication.getPrincipal());
        return bookService.getAllBooks(pageable);
    }

    @GetMapping("/{id}")
    public BookDto getBookId(@PathVariable Long id) {
        return bookService.getById(id);
    }

    @GetMapping("/search")
    public ResponseEntity<Page<BookDto>> searchBooks(@RequestBody SearchRequestDto request, Pageable pageable) {
        return ResponseEntity.ok(bookService.searchByTitle(request, pageable));
    }


    @GetMapping("/author/{authorId}")
    public Page<BookDto> getBooksByAuthorId(@PathVariable Long authorId, Pageable pageable) {
        return bookService.getBooksByAuthorId(authorId, pageable);
    }

    @GetMapping("/publisher/{publisherId}")
    public Page<BookDto> getBooksByPublisherId(@PathVariable Long publisherId, Pageable pageable) {
        return bookService.getBooksByPublisherId(publisherId, pageable);
    }
}